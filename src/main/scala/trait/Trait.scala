//4.1.4.1

trait Feline{
  def colour:String
  def sound:String
}

trait BigCat extends Feline {
  override val sound = "roar" //vedo che senza override funziona ugualmente
  // TODO: override o no? 
}

case class Cat(colour:String, food:String) extends Feline {
  val sound = "meow"
}

case class Tigers(colour:String) extends BigCat {}

case class Lion(colour:String, maneSize:Int) extends BigCat {}

case class Panthers(colour:String) extends BigCat {}

object Main extends App{
  println(Cat("white", "crocks").sound)
  println(Tigers("black").sound)
}

/*solution 2: GERARCHIA DI INTERFACCE
trait Feline{
  def colour:String
  def sound:String
}

case class Cat(colour:String, food:String) extends Feline {
  val sound = "meow"
}

case class Tigers(colour:String) extends Feline {
  val sound = "roar"

}

case class Lion(colour:String, maneSize:Int) extends Feline {
  val sound = "roar"

}

case class Panthers(colour:String) extends Feline {
  val sound = "roar"
}
 */

//4.1.4.2 - 4.2.2.1 (aggiunto sealed a Shapes)
sealed trait Shapes{
  def sides:Int
  def perimeter:Double
  def area:Double
}

case class Circle(radius:Double) extends Shapes{
  override val sides: Int = 1
  override val perimeter: Double = radius * 2 * Math.PI
  override val area: Double = Math.PI * (radius * radius)
}

case class Rectangle(longSide:Double, shortSide:Double) extends Shapes{
  override val sides: Int = 4
  override val perimeter: Double = (longSide*2) + (shortSide*2)
  override val area: Double = longSide * shortSide
}

case class Square(sideLength : Double) extends Shapes{
  override val sides: Int = 4
  override val perimeter: Double = sideLength * sides
  override val area: Double = sideLength * sideLength
}

//4.1.4.3
trait Rectangular extends Shapes{
  override val sides : Int = 4
  def width: Double
  def height: Double
  def perimeter: Double = (width*2) + (height*2)
  override val area: Double = width * height
}

case class Rectangle2(width:Double, height:Double) extends Rectangular{}

case class Square2(sideLength : Double) extends Rectangular{
  val width: Double = sideLength
  val height: Double = sideLength
}

//4.2.2.1

object Draw{
  def apply(shapes: Shapes): String = shapes match {
      case Rectangle(w,h) => s"A rectangle f width ${w} and height ${h}"
      case Circle(r) => s"A circle of radius ${r}"
      case Square(s) => s"A square of size ${s}"
  }
}
//4.2.2.2 saltato perché troppo lungo

//4.2.2.3
object divide{
  def apply(num: Int, den:Int): DivisionResult = if(den==0) Infinite else Finite(num/den)
}

sealed trait DivisionResult
final case class Finite(result: Double) extends DivisionResult
case object Infinite extends DivisionResult

object Main4 extends App{
  divide(2,3) match {
    case Finite(_) => "Risultato della divisione finiito"
  }
}
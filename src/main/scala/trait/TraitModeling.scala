package `trait`

object Main5 extends App {

}

// 4.4.4.1 - Stop on a Dime
//A traffic light is red, green, or yellow. Translate this descrip􏰁on into Scala code.

sealed trait TrafficLight
case object Green extends TrafficLight
case object Yellow extends TrafficLight
case object Red extends TrafficLight

// 4.4.4.2 - Calculator
//A calcul􏰁ation may succeed (with an Int result) or fail (with a String message). Implement this.

sealed trait CalculatorResult
final case class Success(result:Int) extends CalculatorResult
final case class Fail(message:String) extends CalculatorResult

// 4.4.4.3 - Water, Water, Everywhere
//Bo􏰂ttled water has a size (an Int), a source (which is a well, spring, or tap), and a Boolean carbonated. Implement this in Scala.

case class BottledWater(size:Int, source: Source, carbonated:Boolean)
sealed trait Source
case object Well extends Source
case object Spring extends Source
case object Tap extends Source

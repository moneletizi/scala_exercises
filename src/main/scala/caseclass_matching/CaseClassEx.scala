package caseclass_matching

import basic.SimpleCounter

object Main3 extends App{
    println(Persona("Simone", "Letizi").name)
    //not necessary call val in field (in constructor)
    println(Persona("Simone", "Letizi").firstName)
    val p1 = Persona("Mario", "Rossi")
    println(p1)
    val p2 = p1.copy(firstName = "Cane")
    println(p2)
    //test == , eq, equals
    val person1 = Persona("Nome", "Cognome")
    val person2 = Persona("Nome", "Cognome")
    println("eq "+ person1.eq(person2))
    println("= " + (person1 == person2))
    println("equals "+ person1.equals(person1))
  }

/*
  VANTAGGI:
  - non necessita di mettere val nel costruttore
  - non necessita l'uso
  - equals, hashcode, toString
  - metodo copy che crea un nuovo oggetto copia con gli stessi valori
  (The copy method actually accepts op􏰀onal parameters matching each of the constructor parameters.
  If a parameter is specified the new object uses that value instead of the exis􏰀ng value from the current object)
  - =, eq, equals  => eq si comporta come == in java, controllando se sono gli stessi riferimenti; == in scala richiama invece equals

 */
  case class Persona(firstName:String, lastName:String){
    def name = firstName + " " + lastName
  }

  //3.4.5.1 Case Cats
  case class Cat(colour:String, food:String)

  //3.4.5.2 Film and Director
  case class Film(name:String, yearOfRelease:Int, imdbRating:Double, director: Director){
    def directorAge = director.yearOfBirth - yearOfRelease
    def isDirectedBy(director: Director) = this.director == director
  }

  object Film{
    def newer(film1: Film, film2: Film): Film ={
      if (film1.yearOfRelease < film2.yearOfRelease) film1 else film2
    }

    def highestRating(film1: Film, film2: Film): Double = { val rating1 = film1.imdbRating
      val rating2 = film2.imdbRating
      if (rating1 > rating2) rating1 else rating2
    }

    def oldestDirectorAtTheTime(film1: Film, film2: Film): Director = if (film1.directorAge > film2.directorAge) film1.director else film2.director

  }

  case class Director(firstName:String, lastName:String, yearOfBirth:Int){
    def olderThan(director: Director) = if(yearOfBirth < director.yearOfBirth) true else false
  }

  object Director{
    def older(dir1:Director, dir2:Director) : Director = {
      if(dir1.yearOfBirth < dir2.yearOfBirth) dir1 else dir2
    }
  }

  //3.4.5.3 Counter case class
  /*case class SimpleCounter(value:Int = 0){
    def inc = new SimpleCounter(value+1)
    def dec = new SimpleCounter(value-1)
  }
  ERRORE:USARE COPY*/

  //3.4.5.3 Counter case class
  case class SimpleCounter(value:Int = 0){
    def inc = copy(value = value + 1)
    def dec = copy(value = value - 1)
  }

  //3.4.5.4 - Case class e companion object: creiamo il companion object per la case class Persona in cima al file
  //possiamo fare comunque il gioco dei costruttori
  object Persona{
    def apply(name:String): Persona = {
      val splittedName = name.split(" ")
      apply(splittedName(0), splittedName(1))
    }
  }

  object MainPersona extends App {
    println(Persona("Simone Letizi").firstName)
  }

  //Pattern Matching
  //3.5.3.1
  object ChipShop{
    def willServe(cat: Cat) : Boolean = cat.food match {
      case "Chips" => true
      case _ => false
    }

    // TODO: 1 or 2? 
    def willServe2(cat: Cat) : Boolean = cat match {
      case Cat(_,"Chips") => true
      case _ => false
    }
  }

  //3.5.3.2
  object Dad{
    def rate(film: Film) = film match {
      case Film(_,_,_,Director("Clint", "Eastwood",_)) => 10.0
      case Film(_,_,_,Director("John", "McTiernan",_)) => 7.0
      case Film(_,_,_,_) => 3.0 //oppure _ => 3.0
      // TODO: fare un case con film.director? 
    }
  }

  object MainPatternMatching extends App {
    println(ChipShop.willServe(Cat("red","Chips")))
  }



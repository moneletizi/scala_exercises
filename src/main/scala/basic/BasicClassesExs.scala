package basic

  //exs Book Essential Scala page 65
  class Cat(val name:String = "CatName", val colour:String = "White", val food:String)

  object ChipShop{
    def willServe(cat: Cat): Boolean = cat.food.equals("chips")
  }


  class Director(val firstName: String, val lastName:String, val yearOfBirth:Int){
    def name = firstName+" "+lastName
  }

  class Film(val name:String, val yearsOfRelease:Int, val imdbRating:Double,val director:Director) {
    def directorAge = yearsOfRelease - director.yearOfBirth

    def isDirectedBy(director: Director) = this.director.equals(director)


    def copy(name: String = this.name,
             yearsOfRelease: Int = this.yearsOfRelease,
             imbdRating: Double = this.imdbRating,
             director: Director = this.director): Film = new Film(name, yearsOfRelease, imbdRating, director)
  }

  //3.1.6.4.
  class SimpleCounter(val value:Int){
    def inc = new SimpleCounter(value+1)
    def dec = new SimpleCounter(value-1)
  }
  //3.1.6.5.
  class FastingCounter(val value:Int){
    def inc(value:Int = 1) = new FastingCounter(this.value+value)
    def dec(value:Int = 1) = new FastingCounter(this.value-value)
    ////3.1.6.6 extension
    def adjust(adder: Adder) = new FastingCounter(adder.add(this.value))
  }

  //3.1.6.6
  class Adder(amount: Int) {
    def add(in: Int) = in + amount
  }

  //CompanionObject: They are commonly used to provide addi􏰆tional constructors.
  //3.3.2.1
  class Person(val firstName:String, val lastName:String)
  object Person{
    def apply(name:String): Person = {
      val splitted = name.split(" ")
      new Person(splitted(0),splitted(1))
    }
  }

  //3.2.2.2
  object Director{
    def apply(firstName: String, lastName:String, yearOfBirth:Int) = new Director(firstName, lastName, yearOfBirth)
    def older(dir1: Director, dir2:Director) : Director = if (dir1.yearOfBirth > dir2.yearOfBirth) dir1 else dir2
  }

  object Film{
    def apply(name:String, yearsOfRelease:Int, imdbRating:Double, director:Director): Film = new Film(name, yearsOfRelease, imdbRating, director)
    def highestRating(film1: Film, film2:Film) = if (film1.imdbRating>film2.imdbRating) film1.imdbRating else film2.imdbRating
    def oldestDirectorAtTheTime(film1: Film, film2:Film) = if (film1.yearsOfRelease - film1.director.yearOfBirth > film2.yearsOfRelease - film2.director.yearOfBirth) film1.director else film2.director
  }

  object Main extends App{
    //essenzial
    val randomCat = new Cat(colour = "name", food="hamburger")
    println(ChipShop.willServe(randomCat))
    println(new Director("Simone", "Letizi", 1994).name)
    println(new SimpleCounter(10).inc.dec.inc.inc.value)
    println(new FastingCounter(10).inc().dec(5).inc(3).inc().value)
    //3.3.2.1
    println(Person("John Doe").firstName)
  }






package basic

  class Adder2(amount: Int){
    def apply(int: Int) = amount + int
  }


  object Main2 extends App{
    val adder = new Adder2(3)
    //le 2 istruzioni sotto fanno la stessa cosa
    adder(3)
    adder.apply(3)
  }




package function

import java.util

import scala.None
import scala.collection.{View, immutable}

sealed trait Tree[A]{
  def fold[B](mapFun: A=>B)(fun:(B,B) => B) : B = this match {
    case Leaf(value) => mapFun(value)
    case Node(left, right) => fun(left.fold(mapFun)(fun), right.fold(mapFun)(fun))
  }
}
case class Node[A](left:Tree[A], right: Tree[A]) extends Tree[A]
case class Leaf[A](value:A) extends Tree[A]

object Main10 extends App{
  val tree: Tree[String] =
    Node(Node(Leaf("To"), Leaf("iterate")),
      Node(Node(Leaf("is"), Leaf("human,")),
        Node(Leaf("to"), Node(Leaf("recurse"), Leaf("divine")))))

  println(tree.fold[String](identity(_)){(leftRes, rightRes)=> leftRes + " - " + rightRes})

  //5.4.1.1
  val pair = MyPair[String, Int]("hi", 2)
  println(pair.one)
  println(pair.two)

  //es: 5.4.3.1
  Failure[Int, String](1).value
  //// res9: Int = 1
  Success[Int, String]("foo").value // res10: String = foo
  val sum: Sum[Int, String] = Success("foo") // sum: sum.Sum[Int,String] = Right(foo)
  sum match {
    case Failure(x) => x.toString case Success(x) => x
  }

  //5.4.4.1
  val perhaps: Maybe[Int] = Empty[Int]
  val perhaps2: Maybe[Int] = Full(1)

}

//es: 5.4.1.1.
case class MyPair[A,B](one:A, two:B)

//es: 5.4.3.1
// extended with es 5.4.6.3 Folding Sum
//renaming in 5.5.4.4 ex
sealed trait Sum[A,B]{
  def fold[C](mapLeft: A=>C, mapRight: B=>C) : C = this match {
    case Failure(v1) => mapLeft(v1)
    case Success(v2) => mapRight(v2)
  }

  def map[C](mapFunction: B => C) : Sum[A, C] = this match {
    case Failure(v) => Failure(v)
    case Success(v) => Success(mapFunction(v))
  }

  def flatMap[C](flatMapFunction: B => Sum[A, C]) : Sum[A,C] = this match {
    case Failure(v) => Failure(v)
    case Success(v) => flatMapFunction(v)
  }
}
final case class Failure[A,B](value:A) extends Sum[A,B]
final case class Success[A,B](value:B) extends Sum[A,B]

object Main6 extends App{

}





//es: 5.4.4.1 Exercise: Maybe that Was a Mistake
// and 5.4.6.2 Exercise: Folding Maybe

sealed trait Maybe[A]{
  def fold[B](full: A => B, empty: B ) : B = this match {
    case Full(a)=> full(a)
    case Empty() => empty
  }

  //5.5.4.2 Mapping Maybe
  def map[B](mapFunction: A => B) : Maybe[B] = flatMap( a => Full(mapFunction(a)))


  def flatMap[B](flatFunction:A => Maybe[B]) : Maybe[B] = this match {
    case Full(a)=> flatFunction(a)
    case Empty() => Empty[B]()
  }
}
case class Full[A](value:A) extends Maybe[A]
case class Empty[A]() extends Maybe[A]

object Main5 extends App{
  def mightFail1: Maybe[Int] = Full(1)
  def mightFail2: Maybe[Int] = Full(2)
  def mightFail3: Maybe[Int] = Empty() // This one failed
  println(mightFail1.map(Full(_)))
  println(mightFail1.flatMap(Full(_)))
  println(mightFail1 flatMap { x =>
    mightFail2 flatMap { y =>
      mightFail3 flatMap { z =>
        Full(x + y + z)
      } }
  })

  println(mightFail1 flatMap { x =>
    mightFail2 flatMap { y =>
      Full(x + y) }
  })

  //5.5.4.3 Sequencing Computa􏰀tions
  val list = List(1, 2, 3)
  println(list flatMap (a => List(a, -a)))

  val list2: List[Maybe[Int]] = List(Full(3), Full(2), Full(1))
  println(list2.map(a => a.flatMap[Int](x => if(x % 2 == 0) Full(x) else Empty())))
}

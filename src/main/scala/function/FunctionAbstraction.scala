package function

import java.util

import recursive_data.{End, Pair}

//example pag. 110 AND EX. 4.6.3.1
/*sealed trait IntList{
  def length : Int = this match {
    case End => 0
    case Pair(_,t) => 1 + t.length
  }

  def double : IntList = this match {
    case End => this
    case Pair(h, t) => Pair(2*h, t.double)
  }

  def product : Int = this match  {
    case End => 1
    case Pair(h, t) => h * t.product
  }

  def sum : Int = this match {
    case End => 0
    case Pair(h, t) => h + t.sum
  }
}*/

case object End extends IntList {}
final case class Pair(head: Int, tail: IntList) extends IntList {}

sealed trait IntList {
  //5.2.3.1
  //fold first version
  /*def fold(f: (Int,Int) => Int, end: Int): Int = this match {
    case End => end
    case Pair(h,t) => f(h, t.fold(f, end))
  }*/

  //fold generic version
  def fold[E](f: (Int, E) => E, end: E): E = this match {
    case End => end
    case Pair(h, t) => f(h, t.fold(f, end))
  }


  def sum = fold[Int]((v1, v2) => v1 + v2, 0)

  def length = fold[Int]((_, v2) => 1 + v2, 1)

  def product = fold[Int]((v1, v2) => v1 * v2, 1)

  def double = fold[IntList]((h, rec) => Pair(2 * h, rec), this)
}

  //generic fold
  //add one function for each algebraic type
  sealed trait LinkedList[A] {
    def fold[B](end:B, pair: (A,B) => B) : B = this match {
      case GenEnd() => end
      case GenPair(h, t) => pair(h,t.fold(end,pair))
    }

    def foldCarried[B](end: B)(pair: (A, B) => B): B = this match {
      case GenEnd() => end
      case GenPair(hd, tl) => pair(hd, tl.fold(end, pair))
    }

    def map[B](mapFun:A=>B) : LinkedList[B] = this match {
      case GenEnd() => GenEnd()
      case GenPair(h,t) => GenPair(mapFun(h), t.map(mapFun))
    }
  }
  case class GenEnd[A]() extends LinkedList[A]
  case class GenPair[A](h:A,t:LinkedList[A]) extends LinkedList[A]

  object Main extends App {
    val example = GenPair(1, GenPair(2, GenPair(3, GenEnd())))
    println(example.foldCarried(0)((total,elt)=>total+elt))
    // called second parameters with {
    println(example.foldCarried(0){ (total,elt) => total+elt })

    //5.5.4.1 Mapping Lists
    val list: LinkedList[Int] = GenPair(1, GenPair(2, GenPair(3, GenEnd())))
    println(list map { _*2} map {_ + 1} map {_/3})
  }



package polymorphism

  // 4.5.6.1 - Traffic Lights
  /*Using polymorphism and then using pa􏰀ern matching implement a method called next which returns the next TrafficLight in the standard
  Red -> Green -> Yellow -> Red cycle.
  Do you think it is be􏰀er to implement this method inside or outside the class? If inside, would you use pa􏰀ern matching or polymorphism? Why?
   */

  //sol1: polymorphism
  /*
  sealed trait TrafficLight1{
    def next:TrafficLight1
  }
  case object Red1 extends TrafficLight1{
    override def next: TrafficLight1 = Green1
  }

  case object Green1 extends TrafficLight1{
    override def next: TrafficLight1 = Yellow1
  }

  case object Yellow1 extends TrafficLight1{
    override def next: TrafficLight1 = Red1
  }*/

  //sol2: pattern matching outside : bad solution
  /*sealed trait TrafficLight1{}
  object Cycle{
    def next(trafficLight1: TrafficLight1) = trafficLight1 match {
      case Red1 => Green1
      case Green1 => Yellow1
      case Yellow1 => Red1
    }
  }
  case object Red1 extends TrafficLight1
  case object Green1 extends TrafficLight1
  case object Yellow1 extends TrafficLight1*/


  //sol3: match inside : best solution
  sealed trait TrafficLight1{
    def next = this match {
        case Red1 => Green1
        case Green1 => Yellow1
        case Yellow1 => Red1
    }
  }
  case object Red1 extends TrafficLight1
  case object Green1 extends TrafficLight1
  case object Yellow1 extends TrafficLight1

  /*
      4.5.6.2 Calcula􏰁tion
      Create a Calculator object. On Calculator define methods + and - that accept a Calculation and an Int, and return a new Calculation
   */
  sealed trait Calculation
  final case class Success(result: Int) extends Calculation
  final case class Failure(reason: String) extends Calculation
  object Calculator{
    def + (calculation: Calculation, addend: Int) : Calculation = calculation match {
      case Success(value) => Success(value + addend)
      case  Failure(reason) => Failure(reason)
    }

    def - (calculation: Calculation, subtracting: Int) : Calculation = calculation match {
        case Success(value) => Success(value - subtracting)
        case  Failure(reason) => Failure(reason)
    }

    def / (calculation: Calculation, subtracting: Int) : Calculation = (calculation, subtracting) match {
        case (Failure(reason),_) => Failure(reason)
        case (Success(_),0) => Failure("Division by zero")
        case (Success(value),divisor) => Success(value/divisor)
    }

    // TODO: chiedi a paggi quale delle due è meglio
    /*
        def /(calc: Calculation, operand: Int): Calculation = calc match {
          case Success(result) =>
              operand match {
                case 0 => Failure("Division by zero")
                case _ => Success(result / operand)
              }
          case Failure(reason) => Failure(reason)
        }
     */
  }

  object Main extends App{
    assert(Calculator.+(Success(1), 1) == Success(2))
    assert(Calculator.-(Success(1), 1) == Success(0))
    assert(Calculator.+(Failure("Badness"), 1) == Failure("Badness"))
    assert(Calculator./(Success(4), 2) == Success(2))
    assert(Calculator./(Success(4), 0) == Failure("Division by zero"))
    assert(Calculator./(Failure("Badness"), 0) == Failure("Badness"))
  }

  // 4.5.6.3 Email
  sealed trait Visitor
  case object Anonymous extends Visitor
  case object SignUpUser extends Visitor

  object EmailService {
    def email(visitor: Visitor) = visitor match {
      case Anonymous => "" //other info needed SMTP..
      case SignUpUser => ""
    }
  }



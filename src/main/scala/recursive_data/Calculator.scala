package recursive_data

import polymorphism.Calculation

//Es. 4.7.0.1

sealed trait Expression{
  def eval : Calculation = this match {
    case Number(value) => SuccessCalculation(value)

    case Addition(l,r) => (r.eval,l.eval) match {
      case (SuccessCalculation(r1),SuccessCalculation(r2)) => SuccessCalculation(r1+r2)
      case (FailCalculation(description),_) => FailCalculation(description)
      case (_, FailCalculation(description)) => FailCalculation(description)
    }

    case Subtraction(l,r) => (r.eval,l.eval) match {
      case (SuccessCalculation(r1),SuccessCalculation(r2)) => SuccessCalculation(r1-r2)
      case (FailCalculation(description),_) => FailCalculation(description)
      case (_, FailCalculation(description)) => FailCalculation(description)
    }

    case Division(l,r) => l.eval match {
      case SuccessCalculation(leftResult) => r.eval match {
        case SuccessCalculation(0) => FailCalculation("Division by zero")
        case SuccessCalculation(rightResult) => SuccessCalculation(leftResult / rightResult)
        case FailCalculation(reason) => FailCalculation(reason)
      }
      case FailCalculation(reason) => FailCalculation(reason)
    }

    case SquareRoot(e) => e.eval match {
      case SuccessCalculation(ris)  if (ris < 0) => FailCalculation ("Square root of negative number")
      case SuccessCalculation(ris) => SuccessCalculation(ris * 1/2)
      case FailCalculation(reason) => FailCalculation(reason)
    }
  }
}
case class Addition(left : Expression, right : Expression) extends Expression
case class Subtraction(left : Expression, right : Expression) extends Expression
case class Number(value:Double) extends Expression
//2nd stage
case class Division(left : Expression, right : Expression) extends Expression
case class SquareRoot(exp: Expression) extends Expression
//
sealed trait Calculation
case class FailCalculation(description: String) extends Calculation
case class SuccessCalculation(value : Double) extends Calculation

object Main2 extends App{
  assert(Addition(SquareRoot(Number(-1.0)), Number(2.0)).eval == FailCalculation("Square root of negative number"))
  assert(Addition(SquareRoot(Number(4.0)), Number(2.0)).eval == SuccessCalculation (4.0))
  assert(Division(Number(4), Number(0)).eval == FailCalculation("Division by zero"))
}
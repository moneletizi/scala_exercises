package recursive_data
import scala.annotation.tailrec

  //example pag. 110 AND EX. 4.6.3.1
  sealed trait IntList{
    def length : Int = this match {
      case End => 0
      case Pair(_,t) => 1 + t.length
    }

    def double : IntList = this match {
      case End => this
      case Pair(h, t) => Pair(2*h, t.double)
    }
  }

  case object End extends IntList {}
  final case class Pair(head: Int, tail: IntList) extends IntList {}

  @tailrec //to ask the compiler to check that methods we believe are tail recursion really are
  object SumService{
    def sum(list: IntList): Int = list match {
      case End => 0
      case Pair(h,t) => h + sum(t)
    }
  }//non tail

  object SumServiceTail{
    def sum(list: IntList, accumulator: Int = 0): Int = list match {
      case End => accumulator
      case Pair(h,t) => sum(t, accumulator+h)
    }
  }//non tail

  object Main extends App{
    import SumServiceTail._
    val example = Pair(1, Pair(2, Pair(3, End)))
    assert(sum(example) == 6)
    assert(sum(example.tail) == 5)
    assert(sum(End) == 0)

    //4.6.3.1 - 1 part
    assert(example.length == 3)
    assert(example.tail.length == 2)
    assert(End.length == 0)
    //4.6.3.1 - 2 part
    assert(example.double == Pair(2, Pair(4, Pair(6, End))))
    assert(example.tail.double == Pair(4, Pair(6, End)))
    assert(End.double == End)
  }

  // es. 4.6.3.2 solution trait internal (similar to match case in separate object)
  sealed trait Tree{
    def sum: Int = this match {
      case Leaf(value) => value
      case Node(r,l) => r.sum + l.sum
    }

    def double : Tree = this match {
      case Leaf(value) => Leaf(value*2)
      case Node(r,l) => Node(r.double, l.double)
    }
  }
  case class Leaf(int: Int) extends Tree
  case class Node(left:Tree, right:Tree) extends Tree

  /*OTHER SOLUTION:

      PATTERN MATCHING IN SEPARATE OBJECT
      object TreeOps {
        def sum(tree: Tree): Int = tree match {
            case Leaf(elt) => elt
            case Node(l, r) => sum(l) + sum(r)
        }
        def double(tree: Tree): Tree = tree match {
          case Leaf(elt) => Leaf(elt * 2)
          case Node(l, r) => Node(double(l), double(r))
        }
      }

      POLYMORPHISM
      sealed trait Tree {
        def sum: Int
        def double: Tree
      }
      final case class Node(l: Tree, r: Tree) extends Tree {
         def sum: Int = l.sum + r.sum
         def double: Tree = Node(l.double, r.double)
      }
      final case class Leaf(elt: Int) extends Tree {
        def sum: Int = elt
        def double: Tree = Leaf(elt * 2)
      }
   */

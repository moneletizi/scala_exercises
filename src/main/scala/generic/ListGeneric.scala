package generic

sealed trait List[T] {
  def length : Int = this match {
      case End() => 0
      case Pair(h,t) => 1 + t.length
  }

  def contains(element : T) : Boolean = this match {
    case End() => false
    case Pair(h,t) => if (h==element) true else t.contains(element)
  }

  def apply(pos:Int) : Result[T]  = this match {
    case End() => Failure("Index out of bounds")
    case Pair(h,t) => if(pos==0) Success(h) else t.apply(pos-1)
  }
}

sealed trait Result[A]
case class Success[A](value:A) extends Result[A]
case class Failure[A](reason:String) extends Result[A]

case class End[T]() extends List[T]
case class Pair[T](head:T, tail: List[T]) extends List[T]

object Main4 extends App{
  val example = Pair(1, Pair(2, Pair(3, End())))
  assert(example.length == 3)
  assert(example.tail.length == 2)
  assert(End().length == 0)
  assert(example.contains(3) == true)
  assert(example.contains(4) == false)
  assert(End().contains(0) == false)
  // This should not compile
  // example.contains("not an Int")
  assert(example(0) == Success(1))
  assert(example(1) == Success(2))
  assert(example(2) == Success(3))
  assert(example(3) == Failure("Index out of bounds"))
}